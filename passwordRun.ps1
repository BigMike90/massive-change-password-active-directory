Import-Module ActiveDirectory

$password = ConvertTo-SecureString -AsPlainText "Albero21" -Force 
 
$users = Get-Content -Path C:\test.txt
 
ForEach ($user in $users) 
{
    # imposto la password filtrando per nome con -filter
    # Get-ADUser -filter "Name -eq '$user'" | Set-ADAccountPassword -NewPassword $password -Reset
	
	# imposto la password filtrando per username con -identity
    Get-ADUser -Identity $user | Set-ADAccountPassword -NewPassword $password -Reset
    
    # imposto che deve avere la password provvisoria, filtro per nome con -filter
    #Get-ADUser -filter "Name -eq '$user'" | Set-AdUser -ChangePasswordAtLogon $true
	
	# imposto che deve avere la password provvisoria (change password at logon se true provvisoria else no), filtro per username con -identity
    Get-ADUser -Identity $user | Set-AdUser -ChangePasswordAtLogon $true
    
    Write-Host "Utente processato: $user"
}

