# Massive Change password active directory



## Getting started

Va popolato un foglio di test semplice e chiamato **test.txt** con le utenze di dominio complete. Le utenze sono separate da "a capo" per evitare exception d'esecuzione, dopo di che si lancia lo script aprendo powershell active directory. Se AD non è installato non importa viene comunque importata la libreria dei moduli nello script.

## Requisiti
1. Anche se non necessario è fortmente consigliato aver installato active directory (e ovviamente i permessi di visualizzazione e modifica).
2. Mettere script e foglio txt per semplicità su disco **C:\\** anche se è possibile cambiare il source editando lo script
3. Aprire powershell (come admin) e copiare il contenuto del codice sulla shell anzichè eseguire lo script con click destro > esegui come admin
4. Se lo script non riesce a trovare un utente o questo ha caratteri speciali non lo processa e va fatto manualmente


>E' presente del codice commentato per filtrare e trovare utenti per nome/cognome ma è pericoloso in quanto esegue una filter LIKE % > sui recordo, è preferibile lasciare il codice commentato poichè attualmente si filtra per _identity_ (tizio@dominio.it) in quanto univoco. 
